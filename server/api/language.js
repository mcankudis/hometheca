"use strict";
const express = require('express');
const router = express.Router();
const Language = require('../models/Language');

router.get('/', async(req, res) => {
	try {
		let languages = await Language.getAll();
		res.json(languages);
	} catch(err) {
		console.error(new Date(), 'GET [/language/]', err);
        if(err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
	}
})

module.exports = router;