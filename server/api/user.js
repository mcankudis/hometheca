"use strict";
const express = require('express');
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");
const router = express.Router();
const sanitize = require('../functions/sanitize');
const sanitizeBody = require('../functions/sanitizeBody');
const jwtGen = require('../functions/generateTokens');
const ensureAuthenticated = require('../functions/ensureAuthenticated');
const User = require('../models/User');
const Book = require('../models/Book');
const Language = require('../models/Language');

router.post('/register', sanitizeBody, async(req, res) => {
	try {
		if(!req.body.username) throw { msg: "Username must be defined!" };
		if(!req.body.password) throw { msg: "Password must by defined!" };
		if(!req.body.email) throw { msg: "Mail must by defined!" };
		if(!sanitize.verifyEmail(req.body.email)) throw { msg: "Invalid email" };
		
		let user = new User(req.body);
		await user.save();
		res.status(201).json({ msg:"Account created" });
	}
	catch(err) {
		console.error(`[${new Date}, /user/register]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.post('/login', sanitizeBody, async(req, res) => {
	try {
		let user = await User.getByUsername(req.body.username);
		if(!user) throw { msg: "User not found" };
		let validPassword = await User.comparePassword(req.body.password, user.password);
		if(!validPassword) throw { msg: "Invalid password" };
		let { token, reissueToken } = jwtGen.generateTokens(user.id, user.username, user.reissueId);
		res.json({
			user: {
				token,
				reissueToken,
				username: user.username,
				id: user.id,
				locale: user.locale
			}
		});
	}
	catch(err) {
		console.error(`[${new Date}, /user/login]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.get('/token/reissue', async(req, res) => {
	try {
		if(!sanitize.verifyToken(req.headers.authorization)) throw { code: 400, msg: "Invalid input" };
		if(!req.headers.authorization) throw { code: 400, msg: "Invalid input" };
		let tokenData = jwt.verify(req.headers.authorization, process.env.REISSUE_SECRET, { algorithms: ['HS512'] });
		if(typeof tokenData === 'string') throw { code: 403, msg: "Missing credentials" };
		let user = await User.getById(tokenData.id);
		if(!user) throw {code: 404, msg: "User not found"};
		if(tokenData.reissueId !== user.reissueId) throw { code: 403, msg: "Invalid authorization data" };
		let { token, reissueToken } = jwtGen.generateTokens(user.id, user.username, user.reissueId);
		res.json({ token, reissueToken });
	}
	catch(err) {
		console.error(`[${new Date}, /user/token/reissue]`, err);
		if(err) return res.status(err.code || 500).send(err.msg || "Unknown error");
	}
})

router.get('/library', ensureAuthenticated, async(req, res) => {
	try {
		let books = await Book.getByUserId(req.auth.id);
		res.json(books)
	}
	catch(err) {
		console.error(`[${new Date}, /user/library]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.get('/library/recent', ensureAuthenticated, async(req, res) => {
	try {
		let books = await Book.getRecentByUserId(req.auth.id);
		res.json(books)
	}
	catch(err) {
		console.error(`[${new Date}, /user/library/recent]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.get('/library/length', ensureAuthenticated, async(req, res) => {
	try {
		let booksCount = await Book.getCount(req.auth.id);
		res.json({ nBooks: booksCount[0].nBooks })
	}
	catch(err) {
		console.error(`[${new Date}, /user/library/length]`, err);
		if(err.msg) return res.status(400).send(err.msg);
		res.status(500).send("Unknown error");
	}
})

router.post('/password/forgot/', sanitizeBody, async(req, res) => {
	try {
		let user = await User.getByEmail(req.body.email);
		if(!user) throw { code: 404, msg: "User not found" };
		user = new User(user);
		let token = jwtGen.generateResetPasswordToken(user.id, user.username, user.reissueId);
		let transporter = nodemailer.createTransport({
			host: process.env.MAIL_HOST,
			port: 587,
			secure: false,
			auth: {
				user: process.env.MAIL_USER,
				pass: process.env.MAIL_PASSWORD
			},
			tls: {
				rejectUnauthorized: false
			}
		});
		let text = user.getResetPasswordEmailText(token);
		let subject = user.getResetPasswordEmailSubject();
		let mailOptions = {
			from: `"${user.getResetPasswordEmailSenderName()}" <${process.env.MAIL_ADDRESS}>`,
			to: req.body.email,
			subject: subject,
			html: text
		};
		let info = await transporter.sendMail(mailOptions);
		console.log("Message sent: ", info);
		res.status(200).json({ msg: "Email sent" });
	} catch(err) {
		console.error(`[${new Date}, POST /user/password/forgot]`, err);
		if(err) return res.status(err.code || 500).send(err.msg || "Unknown error");
	}
})

router.put('/username', ensureAuthenticated, sanitizeBody, async(req, res) => {
	try {
		let user = await User.getById(req.auth.id);
		if(!user) throw { code: 404, msg: "User not found" };
		user = new User(user);
		await user.changeUsername(req.body.username);
		res.status(200).send({ msg: "Username changed" });
	} catch(err) {
		console.error(`[${new Date}, PUT /user/username]`, err);
		if(err) return res.status(err.code || 400).send(err.msg || "Unknown error");
	}
})

router.post('/password/reset', sanitizeBody, async(req, res) => {
	try {
		let tokenData = jwt.verify(req.body.token, process.env.RESET_PASSWORD_SECRET, { algorithms: ['HS512'] });
		if(!tokenData || typeof tokenData === 'string') throw { code: 403, msg: "Missing credentials" };
		let user = await User.getById(tokenData.id);
		if(!user) throw { code: 404, msg: "User not found" };
		if(user.reissueId !== tokenData.reissueId) throw { code: 403, msg: "Invalid authorization data" };
		user = new User(user);
		await user.changePassword(req.body.password);
		res.status(200).send({ msg: "Password changed" });
	} catch(err) {
		if(err?.message === 'jwt malformed') { err.msg = "Missing permissions"; err.code = 400 }
		console.error(`[${new Date}, POST /user/password/reset]`, err);
		if(err) return res.status(err.code || 400).send(err.msg || "Unknown error");
	}
})

router.put('/language', ensureAuthenticated, sanitizeBody, async(req, res) => {
	try {
		let lang = await Language.getById(req.body.lang);
		if(!lang) throw { code: 404, msg: "Language not found" };
		let user = await User.getById(req.auth.id);
		if(!user) throw { code: 404, msg: "User not found" };
		user = new User(user);
		await user.changeLanguage(lang.id);
		res.status(200).send({ msg: "Language changed" });
	} catch(err) {
		console.error(`[${new Date}, PUT /user/language]`, err);
		if(err) return res.status(err.code || 500).send(err.msg || "Unknown error");
	}
})

module.exports = router;