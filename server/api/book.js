"use strict";
const express = require('express');
const router = express.Router();
const sanitize = require('../functions/sanitize');
const sanitizeBody = require('../functions/sanitizeBody');
const ensureAuthenticated = require('../functions/ensureAuthenticated');
const bookSearch = require('../functions/bookSearch');
const Book = require('../models/Book');

router.get('/find/:isbn', ensureAuthenticated, async(req, res) => {
    try {
        if(req.params.isbn.length !== 10 && req.params.isbn.length !== 13) throw { msg: "Invalid isbn" }
        if(!sanitize.verifyNumber(req.params.isbn)) throw { msg: "Invalid isbn" };
        let found = [], own = [];
        let [resultISBNDB, resultWorldCat, resultBookFinder, resultOwnLibrary] = await Promise.all([
            bookSearch.findOnISBNDB(req.params.isbn),
            bookSearch.findOnWorldCat(req.params.isbn),
            bookSearch.findOnBookFinder(req.params.isbn),
            Book.getByISBN(req.params.isbn, req.auth.id)
        ]);
        if(resultBookFinder.title) found.push(resultBookFinder);
        if(resultISBNDB.title) found.push(resultISBNDB);
        if(resultWorldCat.title) found.push(resultWorldCat);
        resultOwnLibrary.forEach(book => { if(book.title) own.push(book) });
        res.json({ found, own});
        console.log('returned');
    }
    catch(err) {
        console.error(new Date(), 'GET [/book/find/:isbn]', err);
        if(err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
})

router.post('/', ensureAuthenticated, sanitizeBody, async(req, res) => {
    try {
        if(!req.body.title) throw {msg: "Book must have a title!"};
        req.body.userId = req.auth.id;
        console.log(req.body);
        let book = new Book(req.body);
        let id = await book.save();
        book.id = id;
        res.status(201).json(book);
    }
    catch(err) {
        console.error(new Date(), 'POST [/book/new]', err);
        if(err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
})

router.put('/:id', ensureAuthenticated, sanitizeBody, async(req, res) => {
    try {
        if(!sanitize.verifyNumber(req.params.id)) throw { msg: "Invalid book" };
        if(!req.body.title) throw {msg: "Book must have a title!"};
        let book = await Book.getBookById(req.params.id);
        if(book.userId !== req.auth.id) throw { msg: "Missing permissions" };
        book.setProperties(req.body);
        await book.update();
        console.log(book);
        res.status(200).json(book);
    } catch (err) {
        console.error(new Date(), 'PUT [/book/:id]', err);
        if(err && err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
})

router.delete('/:id', ensureAuthenticated, async(req, res) => {
    try {
        if(!sanitize.verifyNumber(req.params.id)) throw { msg: "Invalid book" };
        let book = await Book.getBookById(req.params.id);
        if(book.userId !== req.auth.id) throw { msg: "Missing permissions" };
        await book.delete();
        res.status(200).json(book);
    } catch (err) {
        console.error(new Date(), 'DELETE [/book/:id]', err);
        if(err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
})

router.get('/search/:search', ensureAuthenticated, async(req, res) => {
    try {
        let search = sanitize.sanitize(req.params.search);
        let books = await Book.search(search, req.auth.id);
        res.status(200).json(books);
    } catch (err) {
        console.error(new Date(), 'GET [/book/search/:search]', err);
        if(err && err.msg) return res.status(400).send(err.msg);
        res.status(500).send("Unknown error");
    }
})

module.exports = router;