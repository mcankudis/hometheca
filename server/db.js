"use strict";
const sqlite3 = require('sqlite3').verbose();

let dbpath = process.env.DB_PATH || './db.db';

let db = new sqlite3.Database(dbpath, err => {
	if (err) return console.error(new Date, err.message);
	console.log(new Date, 'Connected to the database.');
});

db.one = async(query) => {
	console.log('executing:', query);
	return new Promise((resolve, reject) => {
		db.get(query, (err, res) => {
			if (err) {
				console.error(new Date, '[db.one]', err);
				reject(0);
			}
			resolve(res);
		})
	})
}

db.many = async() => {
	console.log('executing:', query);
	return new Promise((resolve, reject) => {
		db.all(query, (err, res) => {
			if (err) {
				console.error(new Date, '[db.many]', err);
				reject(0);
			}
			resolve(res);
		})
	})
}

db.execute = async(query) => {
	console.log('executing:', query);
	return new Promise((resolve, reject) => {
		db.run(query, (err) => {
			if(err) {
				console.error(`[${new Date}, db.execute]`, err);
				reject(err);
			}
			resolve();
		});
	})
}

module.exports = db