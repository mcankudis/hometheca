"use strict";
const db = require('../db');

class Language {
    constructor(object) {
        this.id = object.id;
        this.name = object.name;
        this.localName = object.localName;
        this.abbreviation = object.abbreviation;
    }
	
    static getAll() {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM language`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, language/getAll]`, err);
                    reject([]);
                }
                resolve(res);
            });
        })
    }

	static getById = async(id) => {
		return await db.one(`SELECT * from language WHERE id = "${id}"`);
	}
}

module.exports = Language;
