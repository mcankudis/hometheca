"use strict";
const db = require('../db');

class Book {
    constructor(object) {
        this.id = object.id;
        this.title = object.title || "";
        this.isbn13 = object.isbn13 || "";
        this.isbn10 = object.isbn10 || "";
        this.authors = object.authors || "";
        this.publisher = object.publisher || "";
        this.publishDate = object.publishDate || "";
        this.userId = object.userId;
        this.img = object.img || "";
		this.copies = object.copies || 1;
    }

    setProperties({
        title,
        isbn13,
        isbn10,
        authors,
        publisher,
        publishDate,
        img,
		copies
    }) {
        if(title) this.title = title;
        if(isbn13) this.isbn13 = isbn13;
        if(isbn10) this.isbn10 = isbn10;
        if(authors) this.authors = authors;
        if(publisher) this.publisher = publisher;
        if(publishDate) this.publishDate = publishDate;
        if(img) this.img = img;
        if(copies) this.copies = copies;
    }

    save = () => {
        return new Promise((resolve, reject) => {
            let query = `INSERT into book(title, isbn13, isbn10, authors, publisher, publishDate, userId, img) VALUES("${this.title}", "${this.isbn13}", "${this.isbn10}", "${this.authors}", "${this.publisher}", "${this.publishDate}", "${this.userId}", "${this.img}");`;
            console.log("executing:", query);
            db.run(query, function(err) {
                if(err) {
                    console.error(new Date, '[models/book/save]', err);
                    reject(0);
                }
                resolve(this.lastID);
            });
        })
    }

    update() {
        return new Promise((resolve, reject) => {
            let query = `UPDATE book SET
                title = "${this.title}",
                isbn13 = "${this.isbn13}",
                isbn10 = "${this.isbn10}",
                authors = "${this.authors}",
                publisher = "${this.publisher}",
                publishDate = "${this.publishDate}",
                img = "${this.img}",
                copies = "${this.copies}"
                WHERE id = "${this.id}";`;
            console.log('executing:', query);
            db.run(query, (err) => {
                if(err) {
                    console.error(`[${new Date}, book/update]`, err);
                    reject(0);
                }
                resolve();
            });
        })
    }

    delete() {
        return new Promise((resolve, reject) => {
            let query = `DELETE FROM book WHERE id = "${this.id}"`;
            console.log('executing:', query);
            db.run(query, (err, res) => {
                console.log(res);
                if(err) {
                    console.error(`[${new Date}, book/delete]`, err);
                    reject(0);
                }
                resolve(res);
            });
        })
    }

    static search(string, userId) {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM book
                WHERE (
                title LIKE '%${string}%'
                OR isbn13 LIKE '%${string}%'
                OR isbn10 LIKE '%${string}%'
                OR authors LIKE '%${string}%'
                OR publisher LIKE '%${string}%'
                OR publishDate LIKE '%${string}%'
                )
                AND userId = "${userId}";`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/search]`, err);
                    reject([]);
                }
                resolve(res);
            });
        })
    }

    static getBookById(id) {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM book WHERE id = "${id}"`;
            db.get(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/getByUserId]`, err);
                    reject([]);
                }
                resolve(new Book(res));
            });
        })
    }

    static getByUserId(id) {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM book WHERE userId = "${id}"`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/getByUserId]`, err);
                    reject([]);
                }
                resolve(res);
            });
        })
    }

    static getCount(userId) {
        return new Promise((resolve, reject) => {
            let query = `SELECT COUNT(*) as nBooks FROM book WHERE userId = "${userId}";`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/getCount]`, err);
                    reject([]);
                }
                resolve(res);
            });
        })
    }

    static getRecentByUserId(id) {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM book WHERE userId = "${id}" ORDER BY id DESC LIMIT 10`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/getLatestByUserId]`, err);
                    reject();
                }
                resolve(res);
            });
        })
    }

    static getByISBN(isbn, userId) {
        return new Promise((resolve, reject) => {
            let query = `SELECT * FROM book WHERE (isbn13 = "${isbn}" OR isbn10 = "${isbn}") AND userId = "${userId}";`;
            db.all(query, (err, res) => {
                if(err) {
                    console.error(`[${new Date}, book/getByISBN]`, err);
                    reject();
                }
                resolve(res);
            });
        })
    }
}

module.exports = Book;
