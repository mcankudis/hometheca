"use strict";
const db = require('../db');
const bcrypt = require('bcryptjs');
const getRandomString = require('../functions/getRandomString');

class User {
	constructor(object) {
		this.id = object.id;
		this.username = object.username;
		this.password = object.password;
		this.reissueId = object.reissueId;
		this.email = object.email;
		this.language = object.language;
	}
	
	save = () => {
		return new Promise(async (resolve, reject) => {
			let salt = await bcrypt.genSalt(10);
			let hash = await bcrypt.hash(this.password, salt);
			let newReissueId = getRandomString(50, 60);
			let query = `INSERT into user(username, password, email, reissueId, language) VALUES("${this.username}", "${hash}", "${this.email}", "${newReissueId}", "${this.language}");`;
			db.run(query, async (err) => {
				if (err) {
					console.error(new Date, '[models/user/save]', err);
					if (err.errno === 19) {
						if (err.message.includes('username')) reject({ msg: "Username already in use" });
						if (err.message.includes('email')) reject({ msg: "Email already in use" });
					}
					reject(0);
				}
				resolve();
			});
		})
	}

    changeUsername = async(username) => {
		let query = `UPDATE user SET username = "${username}" WHERE id = "${this.id}";`;
		await db.execute(query);
	}

	changePassword = (password) => {
		return new Promise(async (resolve, reject) => {
			let salt = await bcrypt.genSalt(10);
			let hash = await bcrypt.hash(password, salt);
			let newReissueId = getRandomString(50, 60);
			let query = `UPDATE user SET password = "${hash}", reissueId = "${newReissueId}" WHERE id = "${this.id}";`;
			console.log('executing:', query);
			db.run(query, (err) => {
				if(err) {
					console.error(`[${new Date}, user/changePassword]`, err);
					reject(0);
				}
				resolve();
			});
		})
	}

	changeLanguage = async(langId) => {
		let query = `UPDATE user SET language = "${langId}" WHERE id = "${this.id}";`;
		await db.execute(query);
	}

	getResetPasswordEmailSubject = () => {
		switch (this.language) {
			case 1:
				return "Reset hasła";
			case 2:
				return "Passwort neu vergeben";
			default:
				return "Password reset";
		}
	}

	getResetPasswordEmailText = (token) => {
		switch (this.language) {
			case 1:
				return `<b>Cześć ${this.username}</b>
					<p>Ktoś, prawdopodobnie Ty, zarządał resetu hasła do Twojego Księgozbioru.
					Kliknij w poniższy link aby ustawić nowe hasło. Link jest aktywny przez 5 minut.
					</p><a href="${process.env.SERVER_PATH}/login/${token}">
					Reset hasła</a><p>Jeśli to nie Ty chciałeś/aś zmienić hasło,
					skontaktuj się z nami pod <a href="mailto:mikolaj@cankudis.net">tym adresem mailowym</a>
					<br>
					<br>
					Pozdrawiamy,<br>
					Zespół Księgozbioru / Hometheca</p>`;
			case 2:
				return `<b>Lieber ${this.username}</b>
					<p>Jemand, wahrscheinlich du, hat ein neues Passwort für dein Hometheca-Konto angefordert.
					Öffne den unteren Link um ein neues Passwort zu setzen. Bitte beachte, dass der Link nur 5 Minuten lang aktiv ist.</p>
					<a href="${process.env.SERVER_PATH}/login/${token}">
					Neues Passwort setzen</a><p>Fall nicht du diese Email angefordert hast,
					kontaktiere uns bitte unter <a href="mailto:mikolaj@cankudis.net">dieser Email-Adresse
					</a>
					<br>
					<br>
					Mit freundlichen Grüßen,<br>
					Dein Hometheca-Team</p>`;
			default:
				return `<b>Hello ${this.username}</b>
					<p>Someone, probably You, requested a password reset for Your Hometheca account.
					Click the link below to enter a new password. Please notice that the link is only active for 5 minutes.</p>
					<a href="${process.env.SERVER_PATH}/login/${token}">
					Password reset</a><p>In case it was not You who requested a password reset,
					please contact our support at <a href="mailto:mikolaj@cankudis.net">this email adress
					</a>
					<br>
					<br>
					Kind regards,<br>
					your Hometheca-Team</p>`
		}
	}

	getResetPasswordEmailSenderName = () => {
		switch (this.language) {
			case 1:
				return "Menadżer kont Księgozbiór";
			case 2:
				return "Hometheca Konto-Service";
			default:
				return "Hometheca Account Service";
		}
	}

	static getById = async(id) => {
		return new Promise(async (resolve, reject) => {
			db.get(`SELECT * from user WHERE id = "${id}"`, (err, res) => {
				if (err) {
					console.error(new Date, '[models/user/getById]', err);
					reject(0);
				}
				if (!res) resolve();
				resolve(res);
			})
		})
	}

	static getByUsername = async(username) => {
		return new Promise(async (resolve, reject) => {
			db.get(`SELECT user.*, language.abbreviation as locale from user LEFT JOIN language ON user.language = language.id WHERE username = "${username}"`, (err, res) => {
				if (err) {
					console.error(new Date, '[models/user/getByUsername]', err);
					reject(0);
				}
				if (!res) resolve();
				resolve(res);
			})
		})
	}

	static getByEmail = async(email) => {
		return new Promise(async (resolve, reject) => {
			db.get(`SELECT * from user WHERE email = "${email}"`, (err, res) => {
				if (err) {
					console.error(new Date, '[models/user/getByEmail]', err);
					reject(0);
				}
				if (!res) resolve();
				resolve(res);
			})
		})
	}
	
	static comparePassword = async (password, userPassword) => {
		try {
			const isMatch = await bcrypt.compare(password, userPassword);
			if (!isMatch) return false;
			return true;
		} catch (err) {
			console.error('[models/user/comparePassword]', err);
			throw err;
		}
	}
}

module.exports = User;