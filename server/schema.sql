BEGIN TRANSACTION;
DROP TABLE IF EXISTS "book";
CREATE TABLE IF NOT EXISTS "book" (
	"id"	INTEGER UNIQUE,
	"title"	TEXT,
	"isbn13"	TEXT,
	"isbn10"	TEXT,
	"authors"	TEXT,
	"publisher"	TEXT,
	"publishDate"	TEXT,
	"userId"	INTEGER NOT NULL,
	"img"	TEXT,
	"timestampAddedAt"	DATE DEFAULT (datetime('now', 'localtime')),
	"copies"	INTEGER NOT NULL DEFAULT 1,
	PRIMARY KEY("id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "language";
CREATE TABLE IF NOT EXISTS "language" (
	"id"	INTEGER,
	"name"	INTEGER,
	"localName"	INTEGER,
	"abbreviation"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
DROP TABLE IF EXISTS "user";
CREATE TABLE IF NOT EXISTS "user" (
	"id"	INTEGER UNIQUE,
	"username"	TEXT NOT NULL UNIQUE,
	"password"	TEXT NOT NULL,
	"email"	TEXT NOT NULL UNIQUE,
	"reissueId"	TEXT,
	"language"	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("id" AUTOINCREMENT)
);
COMMIT;
