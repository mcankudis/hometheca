'use strict';
const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');

app.use(helmet());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Request logging
const loger = require('./functions/loger');
app.use(loger);

const book = require('./api/book');
const user = require('./api/user');
const language = require('./api/language');
app.use('/book', book);
app.use('/user', user);
app.use('/language', language);

const port = process.env.PORT || 3200;
app.listen(port, () => {
	console.log(new Date, `Hi, listening on port ${port}`);
})
