BEGIN TRANSACTION;
INSERT INTO "language" ("id","name","localName","abbreviation") VALUES (0,'English','English','en'),
 (1,'Polish','Polski','pl'),
 (2,'German','Deutsch','de');
COMMIT;
