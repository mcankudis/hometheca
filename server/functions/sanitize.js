'use strict';
const sanitize = {
	sanitizeObject: obj => {
		let keys = Object.keys(obj);
		keys.forEach(key => obj[key] = sanitize.sanitize(obj[key]));
		return obj;
	},
	// note: this disallows nested objects
	sanitize: a => {
		if(typeof a === 'number' || typeof a === 'boolean') return a;
		if(typeof a !== 'string') return '';
		if(a.length>10000) return '';
		return a.replace(/"/g, "'").replace(/;/g, '');
	},
	verifyToken: a => {
		if(!a || a.length>1000000) return false;
		const noDots = /\./;
		const dots = /\.[^\.]*\./;
		const twoDots = /\.\./;
		const overDots = /\.[^\.]*\.[^\.]*\./;
		const regex = /[^-_.a-z0-9]/i;
		if(regex.test(a)) return false;
		if(!noDots.test(a)) return false;
		if(twoDots.test(a)) return false;
		if(!dots.test(a)) return false;
		return !overDots.test(a);
	},
	verifyNumber: n => {
		if(typeof(n)==="number") return true;
		if(n.length>100000) return false;
		const regex = /[^-.0-9]/i;
		return !regex.test(n);
	},
	verifyEmail: email => {
		if(!email || email==="") return false;
		if(email.length>1000) return false;
		const doubleAt = /@[^@]*@/;
		const at = /@/;
		const dot = /\./;
		const regex = /[^-@._a-zA-Z0-9]/;
		if(regex.test(email)) return false;
		if(!at.test(email)) return false;
		if(!dot.test(email)) return false;
		return !doubleAt.test(email);
	}
}

module.exports = sanitize
