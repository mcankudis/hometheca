"use strict";

const getRandomString = (minSize, maxSize) => {
	let tmp = Math.ceil(Math.random()*(maxSize-minSize))+minSize;
	let helper = "1234567890abcdefghijklmnopqrstuvwxyz";
	let string = '';
	for (let i = 0; i < tmp; i++) {
		let rand = Math.floor(Math.random()*30);
		string+=helper[rand];
	}
	return string;
}

module.exports = getRandomString;