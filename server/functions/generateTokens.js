"use strict";
const jwt = require('jsonwebtoken');
const getRandomString = require('../functions/getRandomString');

const generateTokens = (id, username, reissueId) => {
	let random = getRandomString(40, 60);

	let token = jwt.sign({
		username: username,
		id: id,
		random: random
	}, process.env.SECRET, {algorithm: 'HS512', expiresIn: '10m'});

	random = getRandomString(40, 60);

	let reissueToken = jwt.sign({
		id: id,
		random: random,
		reissueId: reissueId
	}, process.env.REISSUE_SECRET, {algorithm: 'HS512', expiresIn: '7d'});

	return { token, reissueToken };
}

const generateResetPasswordToken = (id, username, reissueId) => {
	let random = getRandomString(40, 60);
	return jwt.sign({
		username: username,
		id: id,
		reissueId: reissueId,
		random: random
	}, process.env.RESET_PASSWORD_SECRET, {algorithm: 'HS512', expiresIn: '5m'});
}

module.exports = { generateTokens, generateResetPasswordToken } ;