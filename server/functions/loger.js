"use strict";

const loger = (req, res, next) => {
	if(req.method === 'GET' && !req.originalUrl.includes('reissue')) return next();
	let bodyClone = Object.assign({}, req.body);
	if(bodyClone.password) bodyClone.password = '';
	console.log(new Date(), req.ip, req.method, req.originalUrl);
	if(req.method === 'POST' || req.method === 'PUT') console.log(bodyClone);
    return next();
}

module.exports = loger;