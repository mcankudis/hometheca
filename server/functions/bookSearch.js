const fetch = require('node-fetch');
const cheerio = require('cheerio');

const findOnISBNDB = async(isbn) => {
	try {
		let url = `https://isbndb.com/book/${isbn}`;
		let data = await fetch(url);
		if(data.status !== 200) throw { msg: "ISBN Fetch failed" };
		data = await data.text();
		let $ = cheerio.load(data, {
			withDomLvl1: true,
			normalizeWhitespace: true,
			xmlMode: false,
			decodeEntities: true
		});
		let img = '';
		img = $('.artwork object').attr('data');
		let info = [];
		$('.book-table table tbody tr')
			.each(function(i,x) {
				$(this)
					.children()
					.each((function(i,x) {
						info.push($(this).text().trim())
					}))
				});
		let object = {};
		for(let i = 0; i<info.length; i++) if(i%2===0) object[info[i]] = info[i+1];
		res = {
			title: object['Full Title'],
			isbn10: object['ISBN'],
			isbn13: object['ISBN13'],
			img: img
		}
		console.log(res);
		return res;
	} catch (err) {
		console.error(new Date(), '[/functions/findOnISBNDB]', err);
		return;
	}
}

const findOnISBNSearch = async(isbn) => {
	try {
		let url = `https://isbnsearch.org/isbn/${isbn}`;
		let data = await fetch(url);
		if(data.status !== 200) throw { msg: "ISBN Fetch failed" };
		data = await data.text();
		let $ = cheerio.load(data, {
			withDomLvl1: true,
			normalizeWhitespace: true,
			xmlMode: false,
			decodeEntities: true
		});
		let img = '';
		img = $('#book img').data;
		let info = [];
		
		bookInfo = Array.from($('.bookinfo').children());
		bookInfo.forEach(el => {
			info.push(el.lastChild.data || el.lastChild.lastChild.data)
		})
		res = {
			title: info[0],
			isbn13: info[1],
			isbn10: info[2],
			authors: info[3],
			publisher: info[4],
			publishDate: info[5],
			img: img
		}
		console.log(res);
		if(res.title) return res;
		return {}
	} catch (err) {
		console.error(new Date(), '[/functions/findOnISBNSearch]', err);
		return {}
	}
}

const findOnWorldCat = async(isbn) => {
	try {
		let url = `https://www.worldcat.org/search?q=${isbn}`;
		let data = await fetch(url);
		if(data.status !== 200) throw { msg: "ISBN Fetch failed" };
		data = await data.text();
		let $ = cheerio.load(data, {
			withDomLvl1: true,
			normalizeWhitespace: true,
			xmlMode: false,
			decodeEntities: true
		});
		let res = {
			title: $('#result-1')[0].children[0].children[0].data,
			authors: $('.author')[0].children[0].data.substring(3),
			publisher: $('.publisher')[0].lastChild.children[0].data,
		}
		if(isbn.length === 13) res.isbn13 = isbn
		if(isbn.length === 10) res.isbn10 = isbn
		console.log(res);
		return res;
	} catch(err) {
		console.error(new Date(), '[/functions/findOnWorldCat]', err);
		return {}
	}
}

const findOnBookFinder = async(isbn) => {
	try {
		let url = `https://www.bookfinder.com/search/?lang=any&isbn=${isbn}&submitBtn=Search&new_used=*&destination=de&currency=EUR&mode=basic&st=sr&ac=qr`;
		let data = await fetch(url);
		if(data.status !== 200) throw { msg: "ISBN Fetch failed" };
		data = await data.text();
		let $ = cheerio.load(data, {
			withDomLvl1: true,
			normalizeWhitespace: true,
			xmlMode: false,
			decodeEntities: true
		});
		let pub = $("[itemprop=publisher]")[0].children[0].data;
		let res = {
			img: $("[itemprop=image]")[0].attribs.src,
			title: $("[itemprop=name]")[0].children[0].data,
			authors: $("[itemprop=author]")[0].children[0].data,
			publisher: pub,
			isbn10: $("[itemprop=isbn]")[0].children[0].data,
			isbn13: $("[itemprop=isbn]")[0].prev.data.substr(0, 13),
			publishDate: pub.substr(pub.length-4, 4)
		}
		console.log(res);
		return res;
	} catch(err) {
		console.error(new Date(), '[/functions/findOnWorldCat]', err);
		return {}
	}
}

module.exports = {
	findOnISBNDB,
	findOnISBNSearch,
	findOnWorldCat,
	findOnBookFinder
}