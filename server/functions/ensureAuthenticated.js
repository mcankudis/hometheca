"use strict";
const sanitize = require('./sanitize');
const jwt = require('jsonwebtoken');

const ensureAuthenticated = (req, res, next) => {
	if(!req.headers.authorization || !sanitize.verifyToken(req.headers.authorization))
		return res.status(401).send("Missing credentials");
	jwt.verify(req.headers.authorization, process.env.SECRET, {algorithms: ['HS512']}, function(err, token) {
		if(err) {
			console.error(`[${new Date}, functions/ensureAuthenticated]`, err);
			return res.status(401).send("Permission denied");
		}
		req.auth = token;
		return next();
	});
}

module.exports = ensureAuthenticated;
