"use strict";
const sanitize = require('../functions/sanitize');

const sanitizeBody = (req, res, next) => {
	req.body = sanitize.sanitizeObject(req.body);
    return next();
}

module.exports = sanitizeBody;