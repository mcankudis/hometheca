module.exports = {
  pwa: {
    name: 'Hometheca',
    themeColor: '#FFFFFF',
    msTileColor: '#8bc34a',
    manifestOptions: {
      background_color: '#8bc34a'
    }
  }
}